import 'package:meta/meta.dart';

class VisitedStore {
  VisitedStore({
    this.id,
    @required this.name,
    this.logo,
    this.phone,
    this.longtitude,
    this.latitude,
    @required this.email,
    @required this.lastSeen
  });

  final String id;
  final String name;
  final String logo;
  final String phone;
  final String longtitude;
  final String latitude;
  final String email;
  final String lastSeen;

  factory VisitedStore.fromJson(Map<String, dynamic> json) => VisitedStore(
    id: json["id"],
    name: json["name"],
    logo: json["logo"],
    phone: json["phone"],
    longtitude: json["longtitude"],
    latitude: json["latitude"],
    email: json["email"],
    lastSeen: json["timestamp"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "logo": logo,
    "phone": phone,
    "longtitude": longtitude,
    "latitude": latitude,
    "email": email,
    "lastSeen": lastSeen
  };
}