class ValidatorService{

  bool validateEmail(String value){
    bool isValid = new RegExp("^[a-zA-Z0-9.!#\$%&\'*+/=?^_\`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*\$").hasMatch(value);
    return isValid;
  }

  bool validatePassword(String value){
    bool isValid = new RegExp("/^(?=.{5})(?=.*\d{2,})/").hasMatch(value);
    return isValid;
  }
}