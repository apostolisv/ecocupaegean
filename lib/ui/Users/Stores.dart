import 'dart:math';

import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:Ecocup/ui/Dialogs/StoresDialogBox.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class Stores extends StatefulWidget{
  Stores({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _StoresState createState() => _StoresState();
}

class _StoresState extends State<Stores> {
  int user_id = 0;
  String name = "";
  String access_token = "";
  List<Store> stores;
  var random = new Random();

  String storesResponse = "";

  @override
  void initState(){
    super.initState();
    _loadCommons();
  }

  Future<String> _loadCommons() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      user_id = prefs.getInt('user_id');
      name = prefs.getString('name');
      access_token = prefs.getString('access_token');
    });
  }

  Future<String> getAllStores() async{
    Map<String,String> body = new Map();
    body.addAll({
      'access_token': access_token,
      'user_id': user_id.toString(),
    });

    String baseUri = await RouteService().getRoute(this.context,"BaseUri");
    String allStoresRoute = await RouteService().getRoute(this.context,"GetAllStores");
    storesResponse = await RestService().doPostRequest(baseUri+allStoresRoute, body);

    setState(() {
      var list = json.decode(storesResponse) as List;
      stores = list.map((i)=>Store.fromJson(i)).toList();
    });
    return storesResponse;
  }

  Widget storesWidget(BuildContext context) {
    return FutureBuilder(
      future: getAllStores(),
      // ignore: missing_return
      builder: (context, snapshot) {
        if (snapshot.hasData || snapshot.connectionState == ConnectionState.done) {
          return
            ListView.separated(
              itemCount: stores.length,
              itemBuilder: (context, index) {
                return new GestureDetector(
                    onTap: () {
                      showDialog(context: context,
                          builder: (BuildContext context){
                            return CustomDialogBox(
                              title: stores[index].name,
                              descriptions: stores[index].phone + "\n" + "Λεωφόρος Νίκης " + random.nextInt(60).toString(),
                              text: "Yes",
                            );
                          }
                      );
                    },
                    child:
                Container(
                  height: 100,
                  child:
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                            width: 70,
                            height: 90,
                            padding: EdgeInsets.only(left:15.0,top:0,bottom: 10),
                            decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              //image: Image.asset('assets/images/ecocup.png'),
                            ),
                            child: Image.asset('assets/images/ecocup.png')
                        ),
                        Column(
                          children: [
                            SizedBox(
                              height: 35,
                            ),
                            Text(stores[index].name,style: TextStyle(fontWeight: FontWeight.w300,color: Colors.blueGrey,fontSize: 20)),
                            SizedBox(
                              height: 5,
                            ),
                          ],
                        ),
                        IconButton(
                          icon: new Icon(Icons.info_outline),
                          iconSize: 30,
                          color: Colors.blueGrey,
                          onPressed: () {showDialog(context: context,
                              builder: (BuildContext context){
                                return CustomDialogBox(
                                  title: stores[index].name,
                                  descriptions: "Hii all this is a custom dialog in flutter and  you will be use in your flutter applications",
                                  text: "Yes",
                                );
                              }
                          );},
                        ),
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
        }
        else if(snapshot.connectionState == ConnectionState.waiting){
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return storesWidget(context);
  }
}

class Store {
  Store({
    this.id,
    this.name,
    this.logo,
    this.phone,
    this.longtitude,
    this.latitude,
    this.email,
    this.auth,
  });

  String id;
  String name;
  String logo;
  String phone;
  String longtitude;
  String latitude;
  String email;
  String auth;

  factory Store.fromJson(Map<String, dynamic> json) => Store(
    id: json["id"],
    name: json["name"],
    logo: json["logo"],
    phone: json["phone"],
    longtitude: json["longtitude"],
    latitude: json["latitude"],
    email: json["email"],
    auth: json["auth"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "logo": logo,
    "phone": phone,
    "longtitude": longtitude,
    "latitude": latitude,
    "email": email,
    "auth": auth,
  };
}
