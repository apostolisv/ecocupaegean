import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';

import 'package:Ecocup/ui/Dialogs/StoresListDialogBox.dart';
import 'package:Ecocup/ui/Dialogs/UserImageConfirmDialog.dart';
import 'package:Ecocup/ui/Users/Settings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_pickers/image_pickers.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../Dialogs/FriendDialogBox.dart';
import 'package:path/path.dart';
import 'package:async/async.dart';

import 'Stores.dart';
import 'package:Ecocup/models/Friend.dart';
import 'package:Ecocup/models/VisitedStore.dart';
import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:Ecocup/ui/Dialogs/StoresDialogBox.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';

class UserProfile extends StatefulWidget {
  UserProfile({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  int user_id = 0;
  String baseUri = "";
  String name = "";
  File user_image;
  String access_token = "";
  String storesResponse = "";
  String messageTitle = "Empty";
  String notificationAlert = "alert";

  List<VisitedStore> _visitedStores = [];

  @override
  void initState() {
    super.initState();
    //_loadCommons();
  }

  Future<void> _loadCommons() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    user_id = prefs.getInt('user_id');
    name = prefs.getString('name');
    access_token = prefs.getString('access_token');
  }

  Widget userDetailsWidget(BuildContext context) {
    return FutureBuilder(
      future: _loadCommons(),
      // ignore: missing_return
      builder: (context, snapshot) {
        if (snapshot.hasData ||
            snapshot.connectionState == ConnectionState.done) {
          return SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Column(
                        children: [
                          Container(
                            width: 150,
                            height: 150,
                            padding: EdgeInsets.only(
                                left: 15.0, right: 15.0, top: 50, bottom: 0),
                            child: Transform.scale(
                              scale: 1.2,
                              child: Center(
                                child: Stack(children: [
                                  Container(
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      border: Border.all(width: 0.5),
                                    ),
                                    child: CircleAvatar(
                                      backgroundImage: user_image == null
                                          ? AssetImage(
                                              'assets/images/ecocup_placeholder_blue.png')
                                          : FileImage(user_image),
                                      radius: 50,
                                    ),
                                  ),
                                  Positioned(
                                    bottom: -14,
                                    right: -9,
                                    child: Transform.scale(
                                      scale: 0.5,
                                      child: Container(
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            shape: BoxShape.circle,
                                          ),
                                          child: Transform.scale(
                                            scale: 2,
                                            child: IconButton(
                                              splashRadius: 14,
                                              onPressed: () =>
                                                  _selectImage(context)
                                                      .then((image) {
                                                if (image != null)
                                                  _showImageConfirmationDialog(
                                                      context, image);
                                              }),
                                              icon: Icon(
                                                  Icons.add_circle_outlined,
                                                  color: Colors.blue),
                                            ),
                                          )),
                                    ),
                                  ),
                                ]),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            name,
                            style: TextStyle(
                                fontSize: 25.0,
                                color: Colors.black,
                                fontWeight: FontWeight.w500),
                          ),
                        ],
                      )
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 25.00),
                    child: _menuWidget(context),
                  ),
                ],
              ),
            ),
          );
        } else if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  Future<File> _selectImage(BuildContext context) async {
    List<Media> _listImagePaths = await ImagePickers.pickerPaths(
        galleryMode: GalleryMode.image,
        selectCount: 1,
        showGif: false,
        showCamera: false,
        compressSize: 200,
        uiConfig: UIConfig(uiThemeColor: Colors.blue),
        cropConfig: CropConfig(enableCrop: false, width: 2, height: 1));

    if (_listImagePaths.length > 0) {
      File image = File(_listImagePaths[0].path);
      return image;
    }
    return null;
  }

  Future<void> _showImageConfirmationDialog(
      BuildContext context, File image) async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return imageConfirmationDialog(context, image, (img) async {
            String response = await _uploadImage(context, image);
            _onImageUploadResponse(response, image);
          });
        });
  }

  Future<String> _uploadImage(BuildContext context, File image) async {
    Map<String, String> body = new Map();
    body.addAll({
      'user_id': user_id.toString(),
    });

    var stream = new http.ByteStream(DelegatingStream.typed(image.openRead()));
    var length = await image.length();
    String baseUri = await RouteService().getRoute(this.context, "BaseUri");
    String imageUpdateRoute =
        await RouteService().getRoute(this.context, "UserImageUpdateRoute");

    var multipartFileImage = new http.MultipartFile('file', stream, length,
        filename: basename(image.path));
    var responseString = await RestService()
        .doPostRequest(baseUri + imageUpdateRoute, body, multipartFileImage);

    return Future.value(responseString);
  }

  void _onImageUploadResponse(String responseString, File image) {
    if (responseString.length > 0) {
      Map<String, dynamic> parsedResponse = jsonDecode(responseString);
      if (parsedResponse['status'] == "success") {
        Fluttertoast.showToast(
          msg: "Profile picture updated",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
        ).then((value) {
          setState(() {
            user_image = image;
          });
        });
      } else if (parsedResponse['status'] == "failed") {
        Fluttertoast.showToast(
          msg: "Error updating profile",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
        );
      }
    } else {
      Fluttertoast.showToast(
        msg: "Error updating profile",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
      );
    }
  }

  Widget _menuWidget(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 40, right: 40),
      child: Column(
        children: [
          InkWell(
            onTap: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return StoresListDialogBox(
                      title: "Find Stores",
                      descriptions: "Your transaction was successful",
                      text: "Done",
                    );
                  });
            },
            child: Card(
              elevation: 2,
              child: ListTile(
                leading: Icon(Icons.store),
                title: Text("Stores"),
              ),
            ),
          ),
          InkWell(
            onTap: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return FriendDialogBox(
                      title: "Add a friend",
                      descriptions: "Your transaction was successful",
                      text: "Done",
                    );
                  });
            },
            child: Card(
              elevation: 2,
              child: ListTile(
                leading: Icon(Icons.person),
                title: Text("Friends"),
              ),
            ),
          ),
          InkWell(
            onTap: () {},
            child: Card(
              elevation: 2,
              child: ListTile(
                leading: Icon(Icons.stars_rounded),
                title: Text("Rewards"),
              ),
            ),
          ),
          InkWell(
            onTap: () => Navigator.push(
                context, MaterialPageRoute(builder: (_) => SettingsScreen())),
            child: Card(
              elevation: 2,
              child: ListTile(
                leading: Icon(Icons.settings),
                title: Text("Settings"),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return userDetailsWidget(context);
  }
}
