import 'dart:async';
import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

import 'Stores.dart';

class MapScreen extends StatefulWidget{
  MapScreen({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _MapState createState() => _MapState();
}

class _MapState extends State<MapScreen> {

  Completer<GoogleMapController> _controller = Completer();
  final Geolocator _geolocator = Geolocator();
  static LatLng _center = new LatLng(40.619936, 22.968489);
  var _currentPosition = new Position();
  GoogleMapController mapController;
  final Map<int, Marker> _markers = {};

  int user_id = -1;
  String access_token = "";
  String name = "";
  String storesResponse = "";
  List<Store> stores;
  BitmapDescriptor markerIcon;

  @override
  void initState(){
    super.initState();
    _loadCommons();
    _getCurrentLocation();
    BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(48, 48)), 'assets/my_icon.png')
        .then((onValue) {
      markerIcon = onValue;
    });
  }

  Future<String> _loadCommons() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      user_id = prefs.getInt('user_id');
      name = prefs.getString('name');
      access_token = prefs.getString('access_token');
    });
  }


    Future<String> getAllStores() async{
      Map<String,String> body = new Map();
      body.addAll({
        'access_token': access_token,
        'user_id': user_id.toString(),
      });

      String baseUri = await RouteService().getRoute(this.context,"BaseUri");
      String allStoresRoute = await RouteService().getRoute(this.context,"GetAllStores");
      storesResponse = await RestService().doPostRequest(baseUri+allStoresRoute, body);

      setState(() {
        var list = json.decode(storesResponse) as List;
        stores = list.map((i)=>Store.fromJson(i)).toList();
      });
      return storesResponse;
    }

  Future<LatLng>_getCurrentLocation() async {
    await Geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((Position position) async {
      setState(() {
        _currentPosition = position;
        _center = LatLng(_currentPosition.latitude, _currentPosition.longitude);
      });
    }).catchError((e) {
      print(e);
    });
    return Future.value(_center);
  }

  Future<void> _onMapCreated(GoogleMapController controller) async{
    getAllStores().then((value) => setState(() {
      mapController = controller;
      _markers.clear();
      for(int i=0; i<stores.length; i++){
        final marker = Marker(
          markerId: MarkerId(stores[i].name),
          position: LatLng(double.parse(stores[i].latitude), double.parse(stores[i].longtitude)),
          icon: markerIcon,
          infoWindow: InfoWindow(
            title: stores[i].name,
            snippet: stores[i].email,
          ),
        );
        _markers[i] = marker;
      }
    }));
  }

  Widget googleMapWidget(BuildContext context) {
    return FutureBuilder(
      future: _getCurrentLocation(),
      builder: (context, snapshot) {
        if (snapshot.hasData || snapshot.connectionState == ConnectionState.done) {
          return
            GoogleMap(
              initialCameraPosition: CameraPosition(
                target: _center,
                zoom: 20.0,
              ),
              myLocationEnabled: true,
              myLocationButtonEnabled: true,
              mapType: MapType.normal,
              zoomGesturesEnabled: true,
              zoomControlsEnabled: true,
              onMapCreated: _onMapCreated,
              markers: _markers.values.toSet(),
            );
        }
        else {//if(snapshot.connectionState == ConnectionState.waiting){
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: googleMapWidget(context)
    );
  }
}
