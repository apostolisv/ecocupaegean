import 'dart:convert';

import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:Ecocup/services/ValidatorService.dart';
import 'package:Ecocup/ui/Dialogs/ReplaceCodeDialog.dart';
import 'package:Ecocup/ui/Dialogs/UserInfoUpdateDialogs.dart';
import 'package:Ecocup/ui/Users/ChooseLoginPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChangePasswordScreen extends StatefulWidget {
  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  int user_id = 0;
  String name = "";
  String access_token = "";
  final passwordFocusNode1 = FocusNode();
  final passwordFocusNode2 = FocusNode();
  final _formKey = GlobalKey<FormState>();
  bool obscureText1 = true;
  bool obscureText2 = true;
  TextEditingController passwordController1 = TextEditingController();
  TextEditingController passwordController2 = TextEditingController();
  bool _isloading = false;

  @override
  void initState() {
    super.initState();
    _loadCommons();
  }

  @override
  void dispose() {
    passwordFocusNode1.dispose();
    passwordFocusNode2.dispose();
    passwordController1.dispose();
    passwordController2.dispose();
    super.dispose();
  }

  Future<void> _submit() async {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();
    setState(() {
      _isloading = true;
    });
    Map<String, String> body = new Map();
    body.addAll(
        {'user_id': user_id.toString(), 'password': passwordController1.text});

    String baseUri = await RouteService().getRoute(this.context, "BaseUri");
    String passwordUpdateRoute =
        await RouteService().getRoute(this.context, "UserPasswordUpdateRoute");
    var responseString =
        await RestService().doPostRequest(baseUri + passwordUpdateRoute, body);
    setState(() {
      _isloading = false;
    });
    if (responseString.length > 0) {
      Map<String, dynamic> parsedResponse = jsonDecode(responseString);
      if (parsedResponse['status'] == "success") {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return successfulUpdateDialog(context, "Password");
            }).then((value) => Navigator.of(context).pop());
      } else if (parsedResponse['status'] == "failed") {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return failedUpdateDialog(context, "password");
            });
      }
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return failedUpdateDialog(context, "password");
          });
    }
  }

  Future<void> _loadCommons() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      user_id = prefs.getInt('user_id');
      name = prefs.getString('name');
      access_token = prefs.getString('access_token');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          title: const Text('Ecocup', style: TextStyle(color: Colors.white)),
          backgroundColor: Colors.blue,
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return ReplaceCodeDialog(
                            title: "Earn Rewards",
                            descriptions:
                                "Visit RePlace | Resolution app to play and earn a code for your rewards",
                            text: "Done",
                          );
                        });
                  },
                  child: Icon(
                    Icons.code,
                    color: Colors.white,
                  ),
                )),
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                ChooseLoginPage()));
                  },
                  child: Icon(
                    Icons.logout,
                    color: Colors.white,
                  ),
                )),
          ],
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Container(
            constraints: BoxConstraints.expand(),
            child: _isloading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : SingleChildScrollView(
                    padding: EdgeInsets.only(top: 20, left: 20, right: 20),
                    child: Container(
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text("Change Password",
                                  style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold)),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Form(
                                    key: _formKey,
                                    child: Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: TextFormField(
                                            validator: (value) {
                                              if (passwordController1
                                                      .text.length <
                                                  4) {
                                                return "Password is too short!";
                                              }
                                              return null;
                                            },
                                            inputFormatters: [
                                              new LengthLimitingTextInputFormatter(
                                                  14)
                                            ],
                                            controller: passwordController1,
                                            focusNode: passwordFocusNode1,
                                            autocorrect: false,
                                            obscureText: obscureText1,
                                            decoration: InputDecoration(
                                              suffixIcon: IconButton(
                                                  splashRadius: 12,
                                                  onPressed: () {
                                                    setState(() {
                                                      obscureText1 =
                                                          !obscureText1;
                                                    });
                                                  },
                                                  icon: obscureText1
                                                      ? Icon(Icons
                                                          .remove_red_eye_outlined)
                                                      : Icon(Icons
                                                          .remove_red_eye)),
                                              labelText: "New Password",
                                              focusedBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(25.0),
                                                borderSide: BorderSide(
                                                  color: Colors.blue,
                                                ),
                                              ),
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(25.0),
                                                borderSide: BorderSide(
                                                  color: Colors.grey,
                                                  width: 1.0,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: TextFormField(
                                            onFieldSubmitted: (value) {
                                              _formKey.currentState.validate();
                                            },
                                            validator: (value) {
                                              if (value !=
                                                  passwordController1.text) {
                                                return "Passwords do not match!";
                                              }
                                              return null;
                                            },
                                            inputFormatters: [
                                              new LengthLimitingTextInputFormatter(
                                                  14)
                                            ],
                                            controller: passwordController2,
                                            focusNode: passwordFocusNode2,
                                            autocorrect: false,
                                            obscureText: obscureText2,
                                            decoration: InputDecoration(
                                              suffixIcon: IconButton(
                                                  splashRadius: 12,
                                                  onPressed: () {
                                                    setState(() {
                                                      obscureText2 =
                                                          !obscureText2;
                                                    });
                                                  },
                                                  icon: obscureText2
                                                      ? Icon(Icons
                                                          .remove_red_eye_outlined)
                                                      : Icon(Icons
                                                          .remove_red_eye)),
                                              labelText: "Confirm Password",
                                              focusedBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(25.0),
                                                borderSide: BorderSide(
                                                  color: Colors.blue,
                                                ),
                                              ),
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(25.0),
                                                borderSide: BorderSide(
                                                  color: Colors.grey,
                                                  width: 1.0,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 20.0),
                            child: TextButton(
                                style: TextButton.styleFrom(
                                    primary: Colors.white,
                                    backgroundColor: Colors.blue),
                                onPressed: () {
                                  FocusScope.of(context).unfocus();
                                  _submit();
                                },
                                child: Text("Save")),
                          ),
                        ],
                      ),
                    )),
          ),
        ));
  }
}
