import 'package:flutter/material.dart';
import 'package:image_pickers/image_pickers.dart';
import 'dart:io';

AlertDialog successfulUpdateDialog(BuildContext context, String message) {
  return AlertDialog(
    title: Text("Success"),
    content: Container(child: Text("$message updated successfully")),
    actions: [
      TextButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text("OK")),
    ],
  );
}

AlertDialog failedUpdateDialog(BuildContext context, String message) {
  return AlertDialog(
    title: Text("Error"),
    content: Container(child: Text("Error updating ${message.toLowerCase()}")),
    actions: [
      TextButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text("OK")),
    ],
  );
}
