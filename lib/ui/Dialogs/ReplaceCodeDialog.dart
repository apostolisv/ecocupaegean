import 'dart:convert';
import 'dart:ui';
import 'package:Ecocup/main.dart';
import 'package:Ecocup/models/NotificationDto.dart';
import 'package:Ecocup/services/RestService.dart';
import 'package:Ecocup/services/RouteService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ReplaceCodeDialog extends StatefulWidget {
  final String title, descriptions, text;
  final Image img;

  const ReplaceCodeDialog ({Key key, this.title, this.descriptions, this.text, this.img}) : super(key: key);

  @override
  _ReplaceCodeDialogState createState() => _ReplaceCodeDialogState();
}

class _ReplaceCodeDialogState extends State<ReplaceCodeDialog > {

  int user_id = 0;
  String baseUri = "";
  String name = "";
  String access_token = "";
  final _formKey = GlobalKey();
  String replaceCode = "RePlace";
  String enteredCode = "";

  @override
  void initState(){
    super.initState();
    _loadCommons();
  }

  Future<void> _loadCommons() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      user_id = prefs.getInt('user_id');
      name = prefs.getString('name');
      access_token = prefs.getString('access_token');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  final myController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    super.dispose();
  }

  contentBox(context){
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 20,top: 65, right: 20,bottom: 20
          ),
          margin: EdgeInsets.only(top: 0),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(color: Colors.black,offset: Offset(0,10),
                    blurRadius: 10
                ),
              ]
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(height: 45,),
              Text(widget.title,style: TextStyle(fontSize: 22,fontWeight: FontWeight.w600),),
              SizedBox(height: 10,),
              Center(
                child: Text(
                  widget.descriptions,
                  style: TextStyle(fontSize: 18,fontWeight: FontWeight.w400),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 10,),
              Text("https://www.replace-app.com/", style: TextStyle(fontSize: 16,fontWeight: FontWeight.w600),),
              Container(
                child: Column(
                  children: [
                    TextFormField(
                        controller: myController,
                        decoration: InputDecoration(
                            labelText:
                                'Please use your code to earn your rewards'),
                        validator: (value) {
                          setState(() {
                            enteredCode = value;
                          });
                          return "";
                        }),
                  ],
                ),
              ),
              SizedBox(height: 22,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Align(
                      alignment: Alignment.center,
                      child: ElevatedButton(
                        onPressed: (){
                          print("ent " + myController.text + " repl " + replaceCode);
                          if (myController.text == replaceCode) {
                            Fluttertoast.showToast(
                                msg: "Congratulations. Visit RePlace | ReSolution to earn your reward",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.green,
                                textColor: Colors.white,
                                fontSize: 16.0
                            );
                            Navigator.pop(context);
                          }
                          else{
                            Fluttertoast.showToast(
                                msg: "Invalid Code. Try Again.",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.red,
                                textColor: Colors.white,
                                fontSize: 16.0
                            );
                          }
                        },
                        child: Text(
                          'Done',
                          style: TextStyle(color: Colors.blue, fontSize: 25),
                        ),
                        style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.white),shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                                side: BorderSide(color: Colors.blue)
                            )
                        )),
                      )
                  ),
                ],
              )
            ],
          ),
        ),
        Positioned(
          left: 20,
          right: 20,
          top: 10,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: 45,
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(45)),
                child: Image.asset("assets/images/replaceapp.png")
            ),
          ),
        ),
      ],
    );
  }
}